class EntriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_entry,       only: [:create]
  before_action :set_entry_by_id, only: [:edit, :update, :destroy]

  def create
    # Becuase the date is passed from the form separately from the start_time
    # and end_time, some preprocessing is performed on the start_time and end_time
    # params to ensure that the times are saved using the date passed by the form
    # and not the default, which is today's date
    @entry.start_time = process_start_time
    @entry.end_time   = process_end_time

    respond_to do |format|
      if @entry.save
        format.html { return redirect_to root_url, notice: 'Entry successfully created' }
        format.js
        format.json { render json: @entry, status: :created, location: root_url }
      else
        format.js { redirect_to root_url }
        format.html {  redirect_to root_url }
        format.json { render json: @entry.errors, status: :unproccessable_entity }
        #render :json => @entry
      end
    end
  end

  def edit
    @projects = Project.all

    respond_to do |format|
      format.js
    end
  end

  def update
    @entry.start_time = process_start_time
    @entry.end_time   = process_end_time

    @entry.description = description_param
    @entry.project_id  = project_id_param

    respond_to do |format|
      if @entry.save
        format.html { return redirect_to root_url, notice: 'Entry successfully created' }
        format.js
      else
        format.js { redirect_to root_url }
        format.html {  redirect_to root_url }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @entry.destroy
        format.js
        format.html { redirect_to root_url }
      else
        format.js { redirect_to root_url }
        format.html {  redirect_to root_url }
      end
    end
  end

  private

  def set_entry
    @entry = current_user.entries.new(entry_params)
  end

  def set_entry_by_id
    id = params[:id] || params[:entry][:id]
    @entry = current_user.entries.find(id)
  end

  def entry_params
    params.require(:entry).permit(:start_time, :end_time, :project_id, :description)
  end

  def process_start_time
    process_time(start_time.to_s)
  end

  def process_end_time
    process_time(end_time.to_s)
  end

  def process_time(time, date = date_param)
    Chronic.time_class = Time.zone
    Chronic.parse("#{date} at #{time}")
  end

  def project_id_param
    params[:entry][:project_id]
  end

  def description_param
    params[:entry][:description]
  end

  def date_param
    params[:entry][:date]
  end

  def start_time
    params[:entry][:start_time]
  end

  def end_time
    params[:entry][:end_time]
  end
end
