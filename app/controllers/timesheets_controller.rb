class TimesheetsController < ApplicationController
  before_action :authenticate_user!

  def index
    @entry = current_user.entries.new
    @entries = current_user.entries.get_entries_for_week(params[:week_ending])
    @entries = ErrorFinder.find_gaps_and_overlaps(@entries)
    @projects = current_user.projects.all
  end
end
