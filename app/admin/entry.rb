ActiveAdmin.register Entry do
  permit_params :start_time, :end_time, :project_id, :description

  form do |f|
    inputs do
      input :project
      input :start_time
      input :end_time
      input :description
    end
    f.actions
  end
end
