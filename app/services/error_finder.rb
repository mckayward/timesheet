module ErrorFinder
  # This class can take an array of entries and identify gaps and overlaps in the entry times

  def self.find_gaps_and_overlaps(hash_of_entries)
    # takes an array of entries returns it with messages for gaps and overlaps
    hash_of_entries.values.each do |entries|
      entries.each.with_index do |entry, index|

        # The array that this method receives is in reverse chronological order (see Entry.get_entries_for_week) meaning that
        # the next entry in the array represents the next 'earliest' timesheet entry, or the entry that was one entry earlier
        # in the day.
        next_element_index = index + 1

        earlier_entry = entries[next_element_index]
        # entries[next_element_index] will return nil for the last element in the array. This means that earlier_entry will
        # be nil for the earliest entry for the day.

        # do nothing if the entry is the earliest entry of the day
        # or if it's a gap or overlap filler
        next if earlier_entry.nil? || entry.is_filler?

        # Check for overlap
        if entry.start_time < earlier_entry.end_time
          entries.insert(next_element_index, Entry.new_overlap)
        end

        # Check for gap
        if entry.start_time > earlier_entry.end_time
          entries.insert(next_element_index, Entry.new_gap)
        end
      end
    end
    hash_of_entries
  end
end
