class DateRangeService
  # Returns a range of date objects that represent the work week that
  # includes the date passed to the method. Calling the method without
  # parameters will return a range of dates that represent the current work week
  def self.get_week_range(date = nil)
    date = self.parse_date(date)

    date.all_week(week_start) unless date.nil?
  end

  def self.parse_date(date = nil)
    set_chronic_time_zone

    date ||= Time.zone.today.to_date

    Chronic.parse(date)
  end

  # returns a list of <option>'s to be used in a <select>
  def self.recent_week_options(num = 15)
    date = Time.now.end_of_week(week_start)
    weeks = []

    num.times do
      formatted_date = [url_format(date), url_format(date)]
      weeks << formatted_date
      date -= 1.week
    end
    weeks
  end

  def self.url_format(date)
    date.strftime("%m-%d-%Y")
  end

  def self.week_start
    ENV['WEEK_START'].to_sym
  end
  private_class_method

  def self.set_chronic_time_zone
    Chronic.time_class = Time.zone
  end
end
