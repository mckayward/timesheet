class Entry < ActiveRecord::Base
  belongs_to :project
  belongs_to :user

  validates_presence_of :project_id
  validates_presence_of :start_time
  validates_presence_of :end_time

  attr_accessor :date
  attr_accessor :gap
  attr_accessor :overlap

  def entry_date
    start_time.to_date
  end

  def gap?
    gap
  end

  def overlap?
    overlap
  end

  def is_filler?
    gap || overlap
  end

  def hours
    (self.end_time - self.start_time) / 60 / 60
  end

  def to_s
    self.start_time.strftime("%m/%d/%y: %H:%M") << self.end_time.strftime(" - %H:%M")
  end

  def self.get_entries_for_week(date = nil)
    week_range = DateRangeService.get_week_range(date)

    # returns all entries within the week_range set above, and includes projects
    # to avoid N+1
    includes(:project).where(entries: {start_time: week_range}).
      order(:start_time).reverse.group_by  { |e| e.start_time.strftime("%A") }
  end

  def self.new_gap
    e = Entry.new
    e.gap = true
    e
  end

  def self.new_overlap
    e = Entry.new
    e.overlap = true
    e
  end

  private

  def self.sort_entries(array_of_entries)
    entries.each.with_index do |e, i|
    end
  end
end
