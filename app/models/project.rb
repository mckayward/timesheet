class Project < ActiveRecord::Base
  has_many :entries
  has_many :project_assignments
  has_many :users, through: :project_assignments
end
