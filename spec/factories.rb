FactoryGirl.define do
  factory :admin_user do
    email                 'test@example.com'
    password              'password'
    password_confirmation 'password'
  end

  factory :user do
    email                 'test@example.com'
    password              'password'
    password_confirmation 'password'
  end

  factory :entry do
    start_time   Time.current
    end_time     { start_time + 1.hour }
    project_id   1
    description "Working on testing entries"
  end

  factory :project do
    name "secret project"
  end

  factory :project_assignment do
    project_id 1
    user_id    1
  end
end
