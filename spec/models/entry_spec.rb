require 'rails_helper'

RSpec.describe Entry do

  describe '.get_entries_for_week' do

    before do
      @project           = Project.create(name: "PoGo")
      @today             = FactoryGirl.create(:entry, start_time: Time.current)
      @one_week_ago      = FactoryGirl.create(:entry, start_time: 1.week.ago)
      @one_week_from_now = FactoryGirl.create(:entry, start_time: 1.week.from_now)

      @this_week = Entry.get_entries_for_week
    end

    it "returns all the entries from this week" do
      expect(@this_week.values[0].include? @today).to eq(true)
      expect(@this_week.size).to eq(1)
    end

    it "does not include entries outside of this week" do
      expect(@this_week.values.to_a.include? @one_week_ago).to eq(false)
      expect(@this_week.values.to_a.include? @one_week_from_now).to eq(false)
    end
  end

  describe '.entry_date' do
    it "returns the date of the start_time" do
      @entry = FactoryGirl.create(:entry)
      expect(@entry.entry_date).to eq(Date.today)
    end
  end
end
