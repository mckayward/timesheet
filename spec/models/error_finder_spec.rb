require 'rails_helper'
require 'error_finder'

RSpec.describe ErrorFinder, type: :model do
  before do

    @nine_am_to_noon = FactoryGirl.create(:entry, start_time: Chronic.parse("9 am"), end_time: Chronic.parse("noon"))
    @noon_to_one     = FactoryGirl.create(:entry, start_time: Chronic.parse("noon"), end_time: Chronic.parse("1 pm"))
    @one_pm_to_three = FactoryGirl.create(:entry, start_time: Chronic.parse("1 pm"), end_time: Chronic.parse("3 pm"))

    @inputs = [
      # entries are in reverse chronological order
      @one_pm_to_three, @noon_to_one, @nine_am_to_noon
      ]

    @entries = {
      # create a hash similar to the one returned by Entry.get_entires_for_week
      "monday" => @inputs
    }
  end

  context "with overlaps" do
    it "properly identifies overlaps" do
      @overlap_entry = FactoryGirl.create(:entry, start_time: Chronic.parse("5 am"), end_time: Chronic.parse("11 am"))
      @entries["monday"].unshift(@overlap_entry)
      expect(@entries.values.first.size).to eq(4)

      @entries = ErrorFinder::find_gaps_and_overlaps(@entries)

      expect(@entries.values.first.size).to eq(5)
      expect(@entries.values.first[1].overlap?).to eq(true)
    end
  end

  context "with gaps" do
    before do
      @gap_entry = FactoryGirl.create(:entry, start_time: Chronic.parse("5 pm"), end_time: Chronic.parse("8 pm"))
      @entries["monday"].unshift(@gap_entry)
    end

    it "properly identifies gaps" do
      expect(@entries.values.first.size).to eq(4)

      @entries = ErrorFinder::find_gaps_and_overlaps(@entries)

      expect(@entries.values.first.size).to eq(5)
      expect(@entries.values.first[1].gap?).to eq(true)
      expect(@entries.values.first[2].equal? @one_pm_to_three).to eq(true)
    end
  end

    it "does not change class of entries" do
      @entries = ErrorFinder::find_gaps_and_overlaps(@entries)
      expect(@entries.values.first.first.class.to_s).to eq("Entry")
    end

  it "returns a hash" do
    @entries = ErrorFinder::find_gaps_and_overlaps(@entries)
    expect(@entries.class.to_s).to eq("Hash")
  end

  it "does not change class of entries" do
    @entries = ErrorFinder::find_gaps_and_overlaps(@entries)
    expect(@entries.values.first.first.class.to_s).to eq("Entry")
  end

  it "does not change structure of input hash" do
    @entries = ErrorFinder::find_gaps_and_overlaps(@entries)
    expect(@entries.keys.size)  .to eq(1)
    expect(@entries.values.size).to eq(1)
  end

  it "does not change entries" do
    @entries = ErrorFinder::find_gaps_and_overlaps(@entries)
    @inputs.each do |entry|
      expect(@entries.values.first.include? entry).to eq(true)
    end
  end
end
