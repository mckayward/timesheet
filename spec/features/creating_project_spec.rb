require 'rails_helper'

RSpec.describe "creating a project" do
  before do
    # sign in as admin
    admin = FactoryGirl.create(:admin_user)
    login_as(admin, scope: :admin_user)
  end

  it "allows creation of project through active admin" do
    test_project_name = "Code Turtle"

    # project to be created is not yet in the list of projects
    visit admin_projects_url
    expect(page).to_not have_text(test_project_name)

    visit new_admin_project_url

    fill_in("project_name", :with => test_project_name)
    click_on("Create Project")

    # project is now included in list of projects
    visit admin_projects_url
    expect(page).to have_text(test_project_name)
  end
end
