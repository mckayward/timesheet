require "rails_helper"

RSpec.describe "creating an entry"do

  context "through active admin" do
    before do
      # sign in as admin
      admin = FactoryGirl.create(:admin_user)
      login_as(admin, scope: :admin_user)
    end

    it "allows creation of entry through active admin" do
      project = Project.create(name: "Test")
      entry_date = Date.today
      start_time = 1.hour.ago
      end_time = Time.now
      description = "Workin' on a buildin'"

      # ensure project to be created is not yet in the list of projects
      visit admin_entries_url
      expect(page).to_not have_text(description)

      # create entry:
      visit new_admin_entry_url

      # select a project
      select(project.name, from: 'entry_project_id')

      # select start time
      # Use .strftime instead of .min or .hour to avoid ambiguity errors in capybara
      # when matching single digit numbers. (e.g., "9" would cause ambiguity error
      # with "19", while "09" would not)
      select(start_time.strftime("%Y"), from: 'entry_start_time_1i')
      select(start_time.strftime("%B"), from: 'entry_start_time_2i')
      select(start_time.strftime("%-d"),  from: 'entry_start_time_3i')
      select(start_time.strftime("%H"), from: 'entry_start_time_4i')
      select(start_time.strftime("%M"), from: 'entry_start_time_5i')

      # select end time
      select(end_time.strftime("%Y"),  from: 'entry_end_time_1i')
      select(end_time.strftime("%B"),  from: 'entry_end_time_2i')
      select(end_time.strftime("%-d"), from: 'entry_end_time_3i')
      select(end_time.strftime("%H"),  from: 'entry_end_time_4i')
      select(end_time.strftime("%M"),  from: 'entry_end_time_5i')

      # give description
      fill_in('entry_description', :with => description)
      click_on('Create Entry')

      # project is now included in list of projects
      visit admin_entries_url
      expect(page).to have_text(description)
    end

  end
  context 'through the index page' do
    before do
      # sign in
      user = FactoryGirl.create(:user)
      login_as(user, scope: :user)

      @project = FactoryGirl.create(:project)
      assignment = FactoryGirl.create(:project_assignment,
                                      project_id: @project.id, user_id: user.id)
    end

    it 'saves the entry' do
      expect(Entry.all.size).to eq(0)
      visit root_url

      today = Time.zone.today
      fill_in(:entry_date, with: today.strftime('%m/%d/%Y'))
      fill_in(:entry_start_time, with: '10:47 PM')
      fill_in(:entry_end_time,   with: '11 PM')
      select(@project.name, from: 'entry_project_id')

      click_on 'Create Entry'
      expect(Entry.all.size).to eq(1)
      expect(page).to have_text(today.strftime('%b %d %Y'))
      expect(page).to have_text('10:47 PM')
      expect(page).to have_text('11:00 PM')
    end
  end
end
