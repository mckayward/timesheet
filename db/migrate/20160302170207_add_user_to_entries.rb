class AddUserToEntries < ActiveRecord::Migration
  def change
    add_column :entries, :user_id, :integer
    add_foreign_key :entries, :users
  end
end
