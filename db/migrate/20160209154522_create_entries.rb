class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.text :description
      t.integer :project_id

      t.timestamps null: false
    end
  end
end
